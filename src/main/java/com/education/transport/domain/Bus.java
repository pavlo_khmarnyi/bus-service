package com.education.transport.domain;

import lombok.Data;

@Data
public class Bus {
    private Long id;
    private String uuid;
    private String model;
    private String governmentSign;
}
