package com.education.transport.domain;

import lombok.Data;

@Data
public class BusStop {
    private String uuid;
    private String name;
    private Long latitude;
    private Long longitude;
}
