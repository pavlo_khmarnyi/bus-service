package com.education.transport.domain;

import lombok.Data;

@Data
public class Driver {
    private Long id;
    private String firstName;
    private String lastName;
    private String licenseNumber;
}
