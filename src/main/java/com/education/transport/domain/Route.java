package com.education.transport.domain;

import java.util.Date;
import java.util.Map;

import lombok.Data;

@Data
public class Route {
    private Long id;
    private String uuid;
    private String name;
    private Map<String, Date> routeScheduler;
    private RouteDefinition routeDefinition;
}
