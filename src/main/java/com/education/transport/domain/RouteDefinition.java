package com.education.transport.domain;

import java.util.List;

import lombok.Data;

@Data
public class RouteDefinition {
    private Long id;
    private String uuid;
    private String number;
    private List<BusStop> busStops;
}
